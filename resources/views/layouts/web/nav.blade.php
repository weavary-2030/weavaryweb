<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: rgba(33,37,41, 0.9)">
    <div class="container">
        <a class="navbar-brand mb-1" href="{{ route('home.index') }}">
            <img src="{{ asset('images/logo.png') }}" alt="" height="30">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link {!! request()->routeIs('careers.index') ? ' active' : '' !!} " {!! request()->routeIs('careers.index') ? ' aria-current="page"' : '' !!} href="{{ route('careers.index') }}">Careers</a>
                </li>
            </ul>
        </div>
    </div>
</nav>