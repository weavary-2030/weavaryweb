<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">


        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="keywords" content="@yield('keywords', config('app.desc'))">
        <meta name="author" content="@yield('author', config('app.name'))">

        <title>@yield('title', config('app.name'))</title>
        <meta property="og:title" content="@yield('title', config('app.name'))" />

        <meta name="description" content="@yield('description', config('app.desc'))">
        <meta property="og:description" content="@yield('description', config('app.desc'))" />

        @hasSection ('cover_image')
        <meta property="og:image" content="@yield('cover_image')" />
        @else
        <meta property="og:image" content="{{ url('/images/cover.png') }}" />
        @endif
        <meta property="og:image:alt" content="@yield('title', config('app.name'))" />

        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:type"   content="@yield('type', 'website')" />
        <!-- Scripts -->


        <!-- Styles -->
        <link href="{{ asset('css/web.css') }}" rel="stylesheet">

        <script src="{{ asset('js/web.js') }}" ></script>

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.png') }}"/>
        @yield('head')
        @yield('meta')
        @yield('script')

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5G6HN9S');</script>
        <!-- End Google Tag Manager -->
    </head>
    <body class="bg-light">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5G6HN9S"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @include('layouts.web.nav')
    @yield('content')
    @include('layouts.web.footer')
    </body>
</html>