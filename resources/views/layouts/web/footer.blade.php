<footer class="footer bg-dark py-2 text-center text-lg-start">
    <div class="container my-5">
      <div class="row mb-4">
          <div class="col">
              <a href="{{ route('home.index') }}"><img src="{{ asset('/images/logo.png') }}" height="30" alt="Weavary Logo"></a>
          </div>
      </div>
      <div class="row">
          <div class="col">
              <ul class="list-inline">
                  <li class="list-inline-item"><a href="{{ route('careers.index') }}" class="text-decoration-none">Careers</a></li>
              </ul>
          </div>
      </div>
      <div class="row text-light">
          <div class="col-12 col-sm-12 col-md-6">
              <i class="fa fa-copyright" aria-hidden="true"></i> 2015-{{ now()->format('Y') }} {{ env('APP_NAME') }}
          </div>
          <div class="col-12 col-sm-12 col-md-6">
              <ul class="list-inline float-lg-end">
                  <li class="list-inline-item"><a href="https://www.facebook.com/weavary" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  <li class="list-inline-item"><a href="https://www.linkedin.com/company/weavary/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
              </ul>
          </div>
      </div>
  </div>
</footer>
