@extends('layouts.web.master')

@section('content')
<section class="bg-dark" style="background-image: url('{{ asset('images/cover.jpg') }}'); background-repeat: no-repeat; background-size: cover; height:100vh; min-height: 600px">
    <div class="container">
        <div class="pt-5 text-center" style="position: absolute; bottom: 15%; top: 30%; left: 5%; right: 5%; ">
            <h1 class="display-1 moving-text-primary">Develop and grow<br>web <i class="fa fa-plus text-primary" aria-hidden="true"></i> app<br>you <i class="fa fa-heart text-danger" aria-hidden="true"></i></h1>
            <p class="lead text-white">Help businesses develop and grow website and mobile app easily</p>
        </div>
    </div>
</section>
<section id="portfolio" class="bg-light">
    <div class="container py-5">
        <h2 class="display-3">Made Possible by Weavary1</h2>
        <div class="row">
            @foreach ($projects as $project)
            <div class="col-lg-4">
                <a href="{{ empty($project['link']) ? 'javascript:;' : $project['link'] }}" target="{{ empty($project['link']) ? '_self' : '_blank' }}" class="text-decoration-none">
                    <div class="card mb-3">
                        <img src="{{ $project['img']['src'] }}" class="card-img-top" alt="{{ $project['img']['alt'] }}">
                        <div class="card-body text-center">
                            <h5 class="card-title">{{ $project['title'] }}</h5>
                            <p class="card-text">{{ $project['overlay_text'] }}</p>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection