@extends('layouts.web.master')

@section('title')
Careers | {{ config('app.name') }}
@endsection

@section('cover_image')
{{ asset('images/careers-in-penang.jpg') }}
@endsection

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11&appId=1787605271539542';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<section id="career" class="pt-4">
    <div class="container py-5">
        <div class="card bg-dark text-white text-center border-light">
            <img src="{{ asset('images/careers-in-penang.jpg') }}" class="card-img" alt="Careers in Penang">
            <div class="card-img-overlay">
                <h5 class="card-title display-2">Careers</h5>
                <p class="card-text">Love what you do, do what you love.<br />Join us on mission incredible, in Penang.</p>
            </div>
        </div>
        <h2 class="display-2 text-center">Our opening<br /><small><span class="badge bg-secondary">Penang, Malaysia</small></h2>
        
        <div class="card-deck">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Software Developers (Intern/Part Time)</h4>
                    <p class="card-text">You will have practice in <kbd>mobile app development</kbd> and/or <kbd>frontend development</kbd> and/or <kbd>backend development</kbd> and/or <kbd>deployment</kbd></p>
                    <ul class="list-unstyled">
                        <li>Requirement:
                            <ul>
                                <li>Have strong knowledge and deep interest in programming.</li>
                                <li>
                                    Able to work on<br />
                                    - <kbd>Mobile</kbd> (React Native) development OR<br />
                                    - <kbd>Frontend</kbd> (React, Bootstrap, HTML/CSS) development OR<br />
                                    - <kbd>Backend</kbd> (Laravel, API, microservices) development OR<br />
                                    - <kbd>DevOps</kbd> (Testing, Continuous Integration, Server setup, and deployment)</li>

                            </ul>
                        </li>
                        <br>
                        <li>Good to have:
                            <ul>
                                <li>Possess Degree/Diploma in Computer Science, Engineering or any related field.</li>
                                <li>Experience in web and mobile apps development</li>
                                <li>Good understanding of Algorithms and Data Structures</li>
                                <li>Experience with Git and Linux</li>
                                <li>Have knowledge of UI/UX and design</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>


        </div>
        <div class="card-deck">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Product Designer (Intern/Part Time)</h4>
                    <p class="card-text">You will have practice in design - prototyping, UI/UX design and brand identity system.</p>
                    <ul class="list-unstyled">
                        <li>Requirement:
                            <ul>
                                <li>Proficient in Photoshop, Illustrator or any prototyping tools.</li>
                                <li>Good art sense.</li>
                            </ul>
                        </li>
                        <li>Good to have:
                            <ul>
                                <li>Possess Degree/Diploma in Graphic Design, Industrial Design or any related field.</li>
                                <li>Side project experience in graphic design or prototyping for web and app</li>
                                <li>Have knowledge of HTML/CSS and frontend engineering with Javascript</li>
                                <li>Have knowledge of branding</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Sales &amp; Marketing (Intern)</h4>
                    <p class="card-text">You will have practice in marketing strategy and planning - offline &amp; digital marketing.</p>
                    <ul class="list-unstyled">
                        <li>Requirement:
                            <ul>
                                <li>Have deep interest and passionate about marketing and business</li>
                            </ul>
                        </li>
                        <li>Good to have:
                            <ul>
                                <li>Possess Degree/Diploma in Marketing, Business or any related field.</li>
                                <li>Have knowledge of branding</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div> -->
        </div>
    </div>
</section>
<section id="join-us" class="bg-light">
    <div class="container-fluid py-5 text-center">
    <h3 class="display-3">Ready to join us?</h3>
    <p class="lead">
        Email now <code><a href="mailto:career@weavary.com">career@weavary.com</a></code><br />
        or message us
        <div class="fb-page" data-href="https://www.facebook.com/weavary" data-tabs="messages" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/weavary" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/weavary">Weavary</a></blockquote></div>
    </p>
</div>
</section>
@endsection