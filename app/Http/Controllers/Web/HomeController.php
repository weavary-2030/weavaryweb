<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = [
            [
                'title' => 'Artifex Design',
                'img' => [
                    'src' => url('images/artifex-design.png'),
                    'alt' => 'Artifex Design coming soon page',
                ],
                'overlay_text' => 'Coming soon page',
                'link' => false,
                'masonry_category' => 'web',
            ],
            [
                'title' => 'Westlake International School',
                'img' => [
                    'src' => url('images/westlakeschool.png'),
                    'alt' => 'Westlake International School',
                ],
                'overlay_text' => 'Website + CMS',
                'link' => 'http://www.westlakeschool.edu.my/',
                'masonry_category' => 'web',
            ],
            [
                'title' => 'FiberPrint',
                'img' => [
                    'src' => url('images/fiberprint.png'),
                    'alt' => 'FiberPrint',
                ],
                'overlay_text' => 'Website + CMS',
                'link' => false,
                'masonry_category' => 'web',
            ],
            [
                'title' => 'Starlife',
                'img' => [
                    'src' => url('images/starlife.png'),
                    'alt' => 'Starlife',
                ],
                'overlay_text' => 'Website + CMS',
                'link' => 'http://www.starlife.my/',
                'masonry_category' => 'web',
            ],
            [
                'title' => 'Westlake Villas',
                'img' => [
                    'src' => url('images/westlakevillas.png'),
                    'alt' => 'Starlife',
                ],
                'overlay_text' => 'Website',
                'link' => 'http://www.westlakevillas.com.my/',
                'masonry_category' => 'web',
            ],
            [
                'title' => 'Danish House',
                'img' => [
                    'src' => url('images/danishhouse.png'),
                    'alt' => 'Danish House',
                ],
                'overlay_text' => 'Website + CMS + Booking System',
                'link' => 'https://www.danishhouse.com.my/',
                'masonry_category' => 'web',
            ],
        ];
        return view('pages.web.home.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
